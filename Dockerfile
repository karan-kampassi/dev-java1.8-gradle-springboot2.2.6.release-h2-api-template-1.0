FROM openjdk:8-jre-alpine
ADD /src/main/resources/application.properties //

COPY /build/libs/*-0.0.1-SNAPSHOT.jar demo.jar
ENTRYPOINT ["java", "-jar", "/demo.jar"]
